# P4: Mandelbrot

Autor: Daniel Ahedo Garcia

Fecha 21/12/2018

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema

- Host (`lscpu, cpufreq, lshw, htop`):

	- Orden de los bytes:                  Little Endian
	- CPU(s):                              4
	- Hilo(s) de procesamiento por núcleo: 2
	- Núcleo(s) por «socket»:              2
	- «Socket(s)»                          1
	- ID de fabricante:                    GenuineIntel
	- Familia de CPU:                      6
	- Modelo:                              58
	- ombre del modelo:                   Intel(R) Core(TM) i3-3110M CPU @ 2.40GHz

	- Virtualización:                      VT-x
	- Caché L1d:                           32K
	- Caché L1i:                           32K
	- Caché L2:                            256K
	- Caché L3:                            3072K
	
	- RAM 8GB
	- Trabajo sobre disco SSd 240GB

## 2. Diseño e Implementación del Software

    Este software consiste en la generacion de una imagen del fractal de Mandelbrot.
	
	Nuestro cometido sera el de  paralelizar el problema de la generacion de una imagen del
	fractal de Mandelbrot, en un multiprocesador de memoria compartida, mediante el paradigma de programacion OpenMP.
	
	Para ello, en un primer paso nos dirigimos al metodo main.
	Lo primero que voy a paralelizar sera el bucle for que itera sobre los pixels, mediante un #pragma omp for schedule(runtime)
	
	En el siguiente apartado, denominado "Determine the coloring of each pixel" paralelizare del mismo modo el for.
	La reserva de memoria de r, g,b en "set the image data" la realizara solo un hilo, para evitar crear muchas, por tanto utilizo la directiva single.
	
	El ultimo bucle se paraleliza de la misma forma que los anteriores.




## 3. Metodología y desarrollo de las pruebas realizadas

	Dado el alto tiempo de ejecucion (unos 100segundos secuencial) he decidido realizar unicamente dos o tres ejecuciones manuales del codigo en lugar
	de utilizar un script con muchas ejecuciones para el becnchmarking.
	
	Tras ejecutar el codigo varias veces, para el caso secuencial obtenemos un tiempo de ejecucion de unos 98,49 segundos, en cambio, obtenemos un tiempo de ejecucion paralelo de 
	54,499376 segundos.
	
	Por tanto obtenemos  un speedUp de 1,8.


## 4. Discusión

Ejercicio 1:

1. Explica brevemente la funcion que desarrolla cada una de las directivas y funciones de OpenMP que has utilizado.

	- #pragma omp critical: directiva que identifica una seccion de codigo que debe ser ejecutada por un solo proceso a la vez.
	 
	- omp_set_lock(): Funcion que bloquea la ejecucion del codigo por parte de los distintos threas hasta que resulta desbloqueado.
	
	- omp_unset_lock(): desbloquea el codigo.
	

2. Etiqueta todas las variables y explica por que le has asignado esa etiqueta.

    private(i,j,x,y,c) shared(c_max,r,g,b,count)
    
    Al igual que en las practicas anteriores, he otorgado la etiqueta private a las variable que son creadas dentro de la zona paralela y portatno tendran distintas copias en memoria,
    en cambio otorgo shared a aquellas variables creadas antes de la zona paralela y que por tanto solo tiene una unica copia en memoria y se comparte su valor.


Ejercicio 2:

	Comentado en el codigo.


