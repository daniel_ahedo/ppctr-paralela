# P4

## Usage

**Compiling**

```sh
make build
```
g++ -std=c++17 -pthread -o mandelbrot_paralelo mandelbrot1paralelo.c -fopenmp

g++ -std=c++17 -pthread -o mandelbrot_secuencial mandelbrot1secuencial.c -fopenmp

**runnning**

```sh
make run
```

./mandelbrot_paralelo

./mandelbrot_secuencial


**Cleaning**

```sh
make clean
```
