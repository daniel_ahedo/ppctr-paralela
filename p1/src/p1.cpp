
#include<stdio.h>
#include<string.h>//strcmp
#include<iostream>//atoi
#include<thread>//threads

enum operaciones{SUMA, XOR};


int sum_xor(int inicio,int fin, operaciones operacion, double* array){

  //  printf("inicio %d\n", inicio);
  //  printf("fin %d\n", fin);
  for (size_t i = inicio; i < fin; i++) {
    printf("array %f\n", array[i]);
  }
  printf("\n");


  if(operacion==SUMA){
    double suma=0;
    for (size_t i = inicio; i < fin; i++) {
      suma=array[i]+suma;
      printf("suma= %f\n",suma);
    }
    printf("\n");
  }

  if(operacion==XOR){
    double contador=array[0];
    for (size_t i = inicio; i < fin; i++) {
      contador=contador^array[i+1];
      printf("xor= %f\n",contador);
    }
    printf("\n");
  }
}



//argc contiene el numero de argumentos introducidos
//argcv es un array con punteros a dichos caracteres
int main(int argc, char *argv[]) {
  int longitud;
  int nHilos;
  operaciones operacion;


  if (argc!=3 && argc!=5 ){
    printf("error en los datos introducidos\n");
    exit(1);
  }

  longitud=atoi(argv[1]);//cast from char* to int loses precission
  nHilos=atoi(argv[4]);
  printf("longitud del array %d\n", longitud) ;
  if(strcmp(argv[2],"sum")==0){
    operacion=SUMA;
    printf("operacion suma\n");
  }else if(strcmp(argv[2],"xor")==0){
    operacion=XOR;
    printf("operacion xor\n");
  }else{
    printf("error en la operacion introducida\n");
    exit(1);
  }

  double* array =(double*)malloc(longitud*sizeof(double));
  for (int i = 0; i < longitud; i++) {
    array[i]=(double)i;
    // printf("numero %f\n", array[i]);
  }

  int tamanho1;
  int tamanho = longitud/nHilos;
  tamanho1=tamanho;
  if(tamanho*nHilos!=longitud){
    tamanho1=tamanho + longitud-(nHilos*tamanho);
  }
  // printf("tamanho1 %d\n",tamanho1 );
  // printf("tamanho %d\n",tamanho );

  int inicio=0;
  int fin=tamanho1;

  if (argc==3){
    sum_xor(inicio,fin,operacion,array);
    // printf("1 hilos\n");
  }

  if(argc==5){

    if (nHilos >10 || nHilos <1){
      printf("error, nº de hilos erroneo\n");
      exit(1);
    }
    // printf("nº de hilos %d\n", nHilos);

    std::thread threads[nHilos];
    for(auto i=0; i<nHilos; ++i){
      threads[i]= std::thread(sum_xor,inicio,fin,operacion,array);
      inicio=fin;
      fin=inicio+tamanho;

      // printf("inicio %d\n",inicio);
      // printf("fin %d\n", fin);
      // printf("creo un hilo\n ");
    }

    for(auto i=0; i<nHilos; ++i){
      threads[i].join();
    }
  }
  return 0;
}

