# P1: Multithreading en C++


5.2. Preguntas 


1. Indica el tiempo que has necesitado para realizar la práctica y el tiempo invertido en cada una de las secciones de la rúbrica. [Respuesta: ∼6 líneas] 
    
    No sabria decir el tiempo exacto que he empleado, pero sin duda no el suficiente, debido a que solo he implementado el base. 
    Al ver en las primeras semanas que iba retrasado decidi centrarme en el resto de las practicas, y esta se quedo un poco en el olvido.



2. (Base) Explica el particionado aplicado, qué variaciones se te ocurren y posibles efectos. [∼5-10 líneas] 

    Se ha repartido la carga de trabajo entre todos los threads. En caso de que la longitud del array fuese impar el primer thread sera el que coja la mayor carga de trabajo.

3. (Conector) Explica cómo has hecho los benchmarks de overhead, proporciona los resultados (recomendable gráfica) y haz un análisis de los mismos. [∼5-30 líneas de análisis] 


