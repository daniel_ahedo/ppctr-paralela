# P2: MatMul

Autor: Daniel Ahedo Garcia

Fecha 21/12/2018


## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

- Host (`lscpu, cpufreq, lshw, htop`):

	- Orden de los bytes:                  Little Endian
	- CPU(s):                              4
	- Hilo(s) de procesamiento por núcleo: 2
	- Núcleo(s) por «socket»:              2
	- «Socket(s)»                          1
	- ID de fabricante:                    GenuineIntel
	- Familia de CPU:                      6
	- Modelo:                              58
	- ombre del modelo:                   Intel(R) Core(TM) i3-3110M CPU @ 2.40GHz

	- Virtualización:                      VT-x
	- Caché L1d:                           32K
	- Caché L1i:                           32K
	- Caché L2:                            256K
	- Caché L3:                            3072K
	
	- RAM 8GB
	- Trabajo sobre disco SSd 240GB


## 2. Diseño e Implementación del Software

	Esta practica se basa en la paralelizacion de un codigo de multiplicacion de matrices. 

	En el ejercicio 1 paralelizaremos la funcion llamada matmul. Este bloque de codigo cuenta con dos bucles for que realizan la multiplicacion de las matrices.

	Para ello realizamos un "#pragma omp parallel" en dico bloque de codigo. Ponemos el numero de threads al maximo con "numthreads()" y "omp_get_max_threads()".
	Las variables de iteracion de los bucles seran privadas, en cambio, el puntero a matriz pasada por parametro sera compartida("shared") debido a que solo se encuentra una en memoria.
	El codigo de multiplicacion lo paralelizamos mediante varios #pragma omp for (uno para cada for, no incluyendo los for anidados)
	En dicho codigo el primer bucle recorre la matriz resultado y la inicializa con valores 0, y el segundo grupo de bucles multiplica los valores y guarda los resultados.



	En el ejercicio 2 paralelizaremos la funcion llamada matmul_sup. 

	Lo paralelizamos de la misma manera que el metodo anterior, excepto que en este caso, utilizamos un #pragma omp parallel schedule() y utiliizamos distintos tipos, los cuales explico 
	mas concretamente en el apartado 4.

	Por otra parte cabe destacar que este metodo es mas eficiente que el anterior debido a que no calcula las multiplicaciones por 0, esto lo observamos en el ultimo bucle, en el cual las 
	iteraciones de k se reducen en funcion de i.




## 3. Metodología y desarrollo de las pruebas realizadas

	- Benchmarking:
	
		#!/usr/bin/env bash

		fileSeq=resultadosSeq.log
		filePar=resultadosPar.log

		touch $fileSeq
		touch $filePar

		for tamanoMatriz in  1 10 25 50 100 200 400 600 800 1000; do

			echo "Tamano matriz: $tamanoMatriz">>$fileSeq
			echo "Tamano matriz: $tamanoMatriz">>$filePar

				for i in `seq 1 1 10`;
				do

				#warmup
				./matmul_secuencial $tamanoMatriz>>warmup
				./matmul_paralelo $tamanoMatriz>>warmup

				./matmul_secuencial $tamanoMatriz>>warmup
				./matmul_paralelo $tamanoMatriz>>warmup

				#ejecucion
				./matmul_secuencial $tamanoMatriz>>$fileSeq
				./matmul_paralelo $tamanoMatriz>>$filePar

				sleep 1
				done
		done


<img src="images/CapturaP2.PNG" width="700" style="display:block; margin:0 auto;"/>

Mediante el anterior script calculo los distintos tiempos de ejecucion para los dichos tamaños de matriz.

Para cada uno de los tiempos de ejecucion tenemos otro bucle que realiza 10 iteraciones, de las cuales saco el tiempo paralelo y secuencial.

Con estos 10 tiempos calculo la media, con la cual calculare el speedUp para cada uno de los tamaños de matriz.



## 4. Discusión

Ejercicio 1

1. Explica brevemente la función que desarrolla cada una de las directivas y funciones de OpenMP que has utilizado. 
	
	- pragma omp parallel: Define una region paralela, es decir, el codigo que abarque esta funcion 
		sera ejecutado de forma paralela por varios threads.
	
	- pragma omp for: Indica que las iteraciones del bucle for se ejecutaran de forma
		paralela por un grupo de threads
	
	- num_threads():  Clausula que define el numero de threads que ejecutaran la region paralela.
	
	- omp_get_max_threads():  Devuelve el numero maximo de threads que pueden ser utilizados.
	
	- omp_get_wtime(): Funcion que devuelve un valor de doble precision relativo al numero de segundos 
		transcurridos desde el valor inicial del reloj en tiempo real del sistema operativo.
		Se garantiza de esta manera que no cambiara el valor inicial durante la ejecucion del programa.
		
	- schedule(): indica como se dividen las iteraciones del bucle entre los threads del equipo.




2. Etiqueta (OpenMP) todas las variables y explica por qué le has asignado esa etiqueta. 

	#pragma omp parallel num_threads(threads) shared(A,B,C,dim) private (i,j,k)
	
	He puesto como privadas las variables i, j y k debido a que estan definidas dentro de la region paralela y sobre 
	ellas iteraran los diversos bucles.
	
	En cambio, A,B,C,dim las he puesto como shared debido a que son variables que han sido definidas antes del codigo paralelo
	y por tanto deberan tener dentro de la region paralela su respectivo valor.(una unica copia en memoria de su valor)





3. Explica cómo se reparte el trabajo entre los threads del equipo en el código paralelo. 

	En la region paralela disponemos de varios bucles for paralelizados.
	
	Al utilizar la clausula #Pragma omp for, esos bucles se realizaran de forma paralela entre varios threads, los cuales
	se repartiran el trabajo de forma equitativa siempre que sea posible.
	



4. Calcula la ganancia (speedup) obtenido con la paralelización.
    
    A partir de los datos de benchmarking obtenemos la liguiente grafica del speedup en funcion del tamaño de la matriz

    <img src="images/speedupP2.PNG" width="500" style="display:block; margin:0 auto;"/>

    Obtenemos una ganancia de unos dos segundos aproximadamente.




Ejercicio 2


1. ¿Qué diferencias observas con el ejercicio anterior en cuanto a la ganancia obtenida sin la clausula schedule? 
Explica con detalle a qué se debe esta diferencia

	Se observa que se reduce el tiempo de computo. 
	Esto es debido a que en el ejercicio 1 seguia al pie de la letra el metodo de multiplicacion de matrices, en cambio,
	en el ejercicio 2, tiene en cuenta que son matrices triangulares y evita realizar multiplicaciones por 0, ahorrando un tiempo considerable.
	
	
	


2. ¿Cuál de los tres algoritmos de equilibrio de carga obtiene mejor resultado?
Explica por qué ocurre esto, en función de cómo se reparte la carga de trabajo y las operaciones que tiene que realizar cada thread. 


	Tras probar todos los algoritmos, el que mejor resultado ha reportado es el dynamic.
	En mi opinion, esto se debe a que asigna el trabajo de una manera mas optima que el resto.
	
	El static por ejemplo, reparte la carga de trabajo entre todos los threads en funcion de un tamaño fijo, y en este caso habra multiplicaciones 
	que generan mayor carga de trabajo que otras, y por tanto sea menos eficiente.
	
	Por otro lado el guided, utiliza paquetes de mayor tamaño ygenera mas overhead.




3. Para el algoritmo static piensa cuál será el tamaño delbloque óptimo, en función de cómo se reparte la carga de trabajo.

	En mi opinion, la mejor forma de repartir la carga de trabajo seria de forma equitativa entr los distintos threads, de forma que todos 
	lleven aproximadamente la misma cantidad de trabajo.



4. Para el algoritmo dynamic determina experimentalmente el tamaño del bloque óptimo. 
	Para ello mide el tiempo de respuesta y speedup obtenidos para al menos 10 tamaños de bloque diferentes. Presenta una tabla resumen de los resultados y explicar detalladamente estos resultados. 

<img src="images/DynamicP2.PNG" width="900" style="display:block; margin:0 auto;"/>

Segun los datos obtenidos y la grafica implementada, observamos como para un mayor tamaño de bloque, el tiempo de ejecucion paralelo es mayor y por tanto obtenemos un menor speedUp.

5. Explica los resultados del algoritmo guided, en función del reparto de carga de trabajo que realiza.

	El algoritmo guided no es efectivo, debido a que el trabajo no se reparte equitativamente. Como mencione en el punto 2, en el bucle de multiplicacion, para cada iteracion del bucle i
	se reducen las iteraciones del bucle K, por tanto se producen irregularidades.



Ejercicio 3


1. ¿Cómo se comportará? Compáralo con el ejercicio anterior. 

	 Al igual que en el ejercicio 2, este metodo es mas optimo para la multiplicacion de matrices triangulares debido a que no sigue el metodo 
	 clasico de multiplicacion de matrices, sino que evita realizar las innecesarias multiplicaciones por 0.



2. ¿Cuál crees que es el mejor algoritmo de equilibrio en este caso? Explica las diferencias que ves con el ejercicio anterior. 

	En mi opinion, el mejor algoritmo seguira siendo el dynamic, debido a su buen reparto del trabajo.
	Ademas podria decir que el algoritmo guided es mas efectivo respecto al metodo anterior, esto es debido a que no se produce la irregularidad en los bucles mencionada.



3. ¿Cuál es el peor algoritmo para este caso?

	En mi  opionion el peor algoritmo sera el static debido a que repartira el trabajo de borma equitativa entr los threads, pero la velocidad
	de computo de las multiplicaciones sera distinta y por tanto el algoritmo no sera eficiente.


