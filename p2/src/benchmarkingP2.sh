#!/usr/bin/env bash

fileSeq=resultadosSeq.log
filePar=resultadosPar.log

touch $fileSeq
touch $filePar

for tamanoMatriz in  1 10 25 50 100 200 400 600 800 1000; do

	echo "Tamano matriz: $tamanoMatriz">>$fileSeq
	echo "Tamano matriz: $tamanoMatriz">>$filePar

        for i in `seq 1 1 10`;
        do

		#warmup
		./matmul_secuencial $tamanoMatriz>>warmup
		./matmul_paralelo $tamanoMatriz>>warmup

		./matmul_secuencial $tamanoMatriz>>warmup
		./matmul_paralelo $tamanoMatriz>>warmup

		#ejecucion
		./matmul_secuencial $tamanoMatriz>>$fileSeq
		./matmul_paralelo $tamanoMatriz>>$filePar

            sleep 1
        done
done

