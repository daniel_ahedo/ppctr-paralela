#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/times.h>
/*la funcion omp_get_wtime me devolvera un valor de doble precision del tiempo de reloj transcurrido*/
#include <omp.h> // para el omp_get_wtime

#define RAND rand() % 100

void init_mat_sup(int dim, float *M);
void init_mat_inf (int dim, float *M);
void matmul (float *A, float *B, float *C, int dim);
void matmul_sup (float *A, float *B, float *C, int dim);
void matmul_inf ();
void print_matrix (float *M, int dim);

/* Usage: ./matmul <dim> [block_size]*/

int main (int argc, char* argv[]){
	int block_size = 1, dim;
	float *A, *B, *C,*RESUL;

	dim = atoi (argv[1]);
	if (argc == 3) block_size = atoi (argv[2]);

	A=(float*) malloc ((dim*dim) *sizeof(float));
	B=(float*) malloc ((dim*dim) *sizeof(float));
	C=(float*) malloc ((dim*dim) *sizeof(float));
	RESUL=(float*) malloc ((dim*dim) *sizeof(float));

	init_mat_sup(dim,A);
	init_mat_sup(dim,B);
	init_mat_inf(dim,C);

	// print_matrix(A,dim);
	// print_matrix(B,dim);
	// print_matrix(C,dim);

	matmul(A,B,RESUL,dim);
//	print_matrix(RESUL,dim);

	matmul_sup(A,C,RESUL,dim);
//	print_matrix(RESUL,dim);

	exit(0);
}


/**
*Inicializa una matriz triangular superior con números aleatorios entre 0 y 99
* @param dim dimension de la matriz
* @param M puntero a donde se almacenara la matriz generada
*/
void init_mat_sup (int dim, float *M) {
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j <= i){
				M[i*dim+j] = 0.0;
			}
			else{
				M[i*dim+j] = RAND;
			}
		}
	}
}



/**
*Inicializa una matriz triangular inferior con números aleatorios entre 0 y 99
* @param dim dimension de la matriz
* @param M puntero a donde se almacenara la matriz generada
*/
void init_mat_inf (int dim, float *M){
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j >= i){
				M[i*dim+j] = 0.0;
			}else{
				M[i*dim+j] = RAND;
			}
		}
	}
}


/**
* Realiza la multiplicacion de las matrices en memoria
*
* @param A puntero a la primera matriz a multiplicar
* @param B puntero a  la Segunda matriz a multiplicar
* @param C puntero a donde se almacenara el resultado de multiplicar A x B
* @param dim dimension de la matriz
*/
void matmul(float *A, float *B, float *C, int dim){
	int i, j, k;
	double tiempo_inicio, tiempo_fin, tiempo_transcurrido;

	int threads= omp_get_max_threads();
	tiempo_inicio= omp_get_wtime();

	#pragma omp parallel num_threads(threads) shared(A,B,C,dim) private (i,j,k)
	{
		#pragma omp for
		for (i=0; i < dim; i++){
			for (j=0; j < dim; j++){
				C[i*dim+j] = 0.0;
			}
		}

		#pragma omp for
		for (i=0; i < dim; i++){
			for (j=0; j < dim; j++){
				for (k=0; k < dim; k++){
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
				}
			}
		}
	}
	tiempo_fin= omp_get_wtime();
	tiempo_transcurrido=tiempo_fin-tiempo_inicio;
	printf("Tiempo_matmul_Paralelo:  %f\n", tiempo_transcurrido);
}



/**
* Multiplica de forma optimizada una matriz triangular superior por una inferior.
*
* @param A puntero a la primera matriz a multiplicar
* @param B puntero a la Segunda matriz a multiplicar
* @param C puntero a donde se almacenara el resultado de multiplicar A x B
* @param dim dimension de la matriz
*/
void matmul_sup (float *A, float *B, float *C, int dim){
	int i, j, k;
	double tiempo_inicio, tiempo_fin, tiempo_transcurrido;

	int threads= omp_get_max_threads();
	tiempo_inicio= omp_get_wtime();

	#pragma omp parallel num_threads(threads) shared(A,B,C,dim) private (i,j,k)
	{
		#pragma omp for schedule(dynamic,1)
		//#pragma omp for schedule(static)
		//#pragma omp for schedule(guided)
		for (i=0; i < dim; i++){
			for (j=0; j < dim; j++){
				C[i*dim+j] = 0.0;
			}
		}
		#pragma omp for schedule(dynamic,1)
		//#pragma omp for schedule(static)
		//#pragma omp for schedule(guided)
		for (i=0; i < (dim-1); i++){
			for (j=0; j < (dim-1); j++){
				for (k=(i+1); k < dim; k++){
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
				}
			}
		}
	}
	tiempo_fin= omp_get_wtime();
	tiempo_transcurrido=tiempo_fin-tiempo_inicio;
	//printf("Tiempo matmul_sup paralelo: %f\n", tiempo_transcurrido);

}



/**
* Multiplica de forma optimizada una matriz triangular inferior por una superior
*
* @param A puntero a la primera matriz a multiplicar
* @param B puntero a de la Segunda matriz a multiplicar
* @param C puntero a dende se almacenara el resultado de multiplicar A x B
* @param dim dimension de la matriz
*/
void matmul_inf (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
	for (j=0; j < dim; j++)
	C[i*dim+j] = 0.0;

	for (i=1; i < dim; i++)
	for (j=1; j < dim; j++)
	for (k=0; k < i; k++)
	C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
}


/**
* Pinta la matriz dada
*
* @param Matriz puntero a  la matriz a pintar
* @param dim dimension de la matriz
*/
void print_matrix (float *MATRIZ, int dim){
	int i,j,aPintar;
	aPintar=0;
	for (size_t i = 0; i < dim; i++) {
		for (size_t j = 0; j < dim; j++) {
			printf("    %f    ",MATRIZ[aPintar]);
			aPintar++;
		}
		printf("\n");
	}
}
