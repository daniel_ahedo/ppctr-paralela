# P2

## Usage

**Compiling**

```sh
make build
```
g++ -std=c++17 -pthread -o matmul_paralelo matmul_paralelo.c -fopenmp

g++ -std=c++17 -pthread -o matmul_secuencial matmul_secuencial.c -fopenmp

**runnning**

```sh
make run
```

./matmul_secuencial 1000 1 -> matmulSeq.txt

./matmul_paralelo 1000 1 -> matmulPar.txt

diff matmulSeq.txt matmulPar.txt 

**Cleaning**

```sh
make clean
```
