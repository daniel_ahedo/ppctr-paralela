# P3: VideoTask

Autor : Daniel Ahedo Garcia

Fecha 21/12/2018


## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

- Host (`lscpu, cpufreq, lshw, htop`):

	- Orden de los bytes:                  Little Endian
	- CPU(s):                              4
	- Hilo(s) de procesamiento por núcleo: 2
	- Núcleo(s) por «socket»:              2
	- «Socket(s)»                          1
	- ID de fabricante:                    GenuineIntel
	- Familia de CPU:                      6
	- Modelo:                              58
	- ombre del modelo:                   Intel(R) Core(TM) i3-3110M CPU @ 2.40GHz

	- Virtualización:                      VT-x
	- Caché L1d:                           32K
	- Caché L1i:                           32K
	- Caché L2:                            256K
	- Caché L3:                            3072K
	
	- RAM 8GB
	- Trabajo sobre disco SSd 240GB
	
## 2. Diseño e Implementación del Software

	Esta practica se basa en la comprension, optimizacion y  paralelizacion de un codigo filtrado de video. 
	
	Este codigo, coge los datos de un fichero de entrada, los procesa mediante una funcion llamada fgaus, y lo improme en un fichero de salida.

	Tras comprender el codigo, nos damos cuenta que hay un error, debido a que cuando lee los datos del fichero de entrada no itera en funcion de i, por tanto solo
	leera un dato. La solucion ha sido añadir un bucle for para que lea los datos correctamente.(Posteriormente añadire mas for para optimizar el codigo)
	
	El codigo a paralelizar sera el bucle do-while en el que se leen los datos de entrada, se procesan y se almacenan en la salida.
	
	Para ello, utilizamos un parallel, ademas, utilizaremos la directiva single para que solo uno de los hilos pueda realizarlo.
	Este hilo sera el encargado de ejecutar	este bloque de codigo e ir generando tareas (pragma omp task) de fgaus, las cuales seran realizadas por los demas hilos.
	
	Una vez todas las tareas se hayan terminado (pragma omp taskwait), se ponen en el fichero de salida los resultados.
	
	

## 3. Metodología y desarrollo de las pruebas realizadas


    #!/usr/bin/env bash

    fileRes=resultado.log   

    touch $fileRes

    for i in  1 2 3 4 5 6 7 8 9 10; do
    		echo "Tiempo secuencial " $i":">>$fileRes

        		#warmup
        		./video_task_secuencial>>warmup
		
	        	#ejecucion
		        ./video_task_secuencial>>$fileRes

                sleep 1 
    done

    for i in  1 2 3 4 5 6 7 8 9 10; do
	    	echo "Tiempo paralelo " $i":">>$fileRes
		
    		#warmup
		
		    ./video_task_paralelo>>warmup

		    #ejecucion
		
	    	./video_task_paralelo>>$fileRes

            sleep 1
    done

 Mediante el mencionado codigo he conseguido obtener de forma rapida y sencilla los distintos tiempos de ejecucion del programa de video con los parametros por defecto.
 
 Estos tiempos seran utilizados mas tarde para calcular el speedUp y obtener los distintos datos sobre la ejecucion del programa.



## 4. Discusión

Ejercicio 1:


1. Explica que funcionalidad has tenido que an˜adir/modiﬁcar sobre el codigo original. 
   Explica si has encontrado algun fallo en la funcionalidad (“lo que hace el programa”, independientemente al paralelismo) y como lo has corregido.
   
   El primer fallo que he encontrado ha sido que al hacer el bucle do-while, la lectura de los datos de entrada no iteraba en funcion de la i.
   En una primera version lo solucione creando un bucle for que lo englobara, pero no era adecuado debido a que se leia, se procesaba y se escribia un dato por cada iteracion
   y eso no es lo que buscamos.
   
   Lo que nosotros buscamos es leer todos los datos de entrada, despues procesarlos uno a uno, y finalmente escribirlos en el fichero de salida.
   
   Esto lo consiguo realizando 3 bucles for distintos, el primero ocupa solo la lectura del fichero de entrada. Una vez leidos todos los datos, hacemos otro bucle for que 
   haga la llamada al procesamiento con fgaus, y posteriormente otro bucle for que lo ponga en el fichero de salida.



2. Explica brevemente la funcion que desarrolla cada una de las directivas y funciones de OpenMP que has utilizado,
	salvo las que ya hayas explicado en la practica anterior (por ejemplo parallel). 
	
	- pragma omp single: Con la directiva single conseguimos que el bloque de codigo sea ejecutado unicamente por un thread del equipo. Lo ejecuta el primer thread que llega a la directiva
	y esperan al llegar a la barrera implicita.
	
	- pragma omp task: Esta directiva genera tareas de forma explicita. las tareas son unidades de trabajo independientes que se crean y se dejan en una cola a la espera de que las coja un thread libre.
	
	- pragma omp taskwait:  Esta directiva actua como una barrera, es decir, espera a que todas las tareas que han sido creadas a partir de la actual terminen.


3. Etiqueta todas las variables y explica por que le has asignado esa etiqueta. 
	
	 shared (filtered,pixels)
	 
	 A las variables filtered y pixels les otorgo la etiqueta shared debido a que son variables que han sido definidas en memoria antes del codigo paralelo, es decir, solo tienen una copia en memoria
	 y se tienen que compartirse entre los distintos threads.


4. Explica como se consigue el paralelismo en este programa (aplicable sobre la funcion main).

	El bloque de codigo a paralelizar es el do-while (pragma omp parallel).

	En un primer paso este bloque de codigo lo ejecutara un solo hilo(pragma omp single), que sera el que lea los datos de entrada y genere tareas (pragma omp task)de llamadas a fgaus. 
	
	Despues, el resto de hilos que esten libres iran cogiendo estas tareas, e iran computando fgaus.

	Una vez estos hilos terminan de realizar la funcion fgaus, deben esperar a que todos hayan terminado(pragma omp taskwait).ç
	
	Cuando esten todos, se pintan los resultados en el fichero de salida.
	

5. Calcula la ganancia (speedup) obtenido con la paralelizacion y explica los resultados obtenidos

<img src="images/speedupP3.PNG" width="500" style="display:block; margin:0 auto;"/>

Utilizando el script mencionado en el apartado de benchmarking he obtenido 10 tiempos de ejecucion tanto paralalos como secuenciales. 

Utilizando estos tiempos, obtenemos un valor medio, tanto paralelo como secuencial con los cuales calculamos un speedup. El speedup obtenido es de 1,47232732



6. ¿Se te ocurre alguna optimizacion sobre la funcion fgauss? 


Una forma de optimizar el codigo seria mediante desarrollo de bucles (loop unroll), por el cual, para cada iteracion del bucle podriamos realizar varias.

(No lo he llegado a implementar)



