# P3

## Usage

**Compiling**

```sh
make build
```

gcc -o generator generator.c

g++ -std=c++17 -pthread -o video_task_secuencial video_task_secuencial.c -fopenmp

g++ -std=c++17 -pthread -o video_task_paralelo video_task_paralelo.c -fopenmp



**runnning**

```sh
make run
```

./generator

./video_task_secuencial

./video_task_paralelo

diff movie_paralelo.out movie_secuencial.out

**Cleaning**

```sh
make clean
```