#!/usr/bin/env bash

fileRes=resultado.log

touch $fileRes

for i in  1 2 3 4 5 6 7 8 9 10; do
		echo "Tiempo secuencial " $i":">>$fileRes

    		#warmup
		./video_task_secuencial>>warmup
		
		#ejecucion
		./video_task_secuencial>>$fileRes

        sleep 1
done

for i in  1 2 3 4 5 6 7 8 9 10; do
		echo "Tiempo paralelo " $i":">>$fileRes
		
    		#warmup
		
		./video_task_paralelo>>warmup

		#ejecucion
		
		./video_task_paralelo>>$fileRes

        sleep 1
done

